plugin_paths = { "/usr/share/jitsi-meet/prosody-plugins/" }

-- domain mapper options, must at least have domain base set to use the mapper
muc_mapper_domain_base = "192.168.21.102";

external_service_secret = "eV6blqCIL44gCL6y";
external_services = {
     { type = "stun", host = "192.168.21.102", port = 3478 },
     { type = "turn", host = "192.168.21.102", port = 3478, transport = "udp", secret = true, ttl = 86400, algorithm = "turn" },
     { type = "turns", host = "192.168.21.102", port = 5349, transport = "tcp", secret = true, ttl = 86400, algorithm = "turn" }
};

cross_domain_bosh = false;
consider_bosh_secure = true;
-- https_ports = { }; -- Remove this line to prevent listening on port 5284

-- by default prosody 0.12 sends cors headers, if you want to disable it uncomment the following (the config is available on 0.12.1)
--http_cors_override = {
--    bosh = {
--        enabled = false;
--    };
--    websocket = {
--        enabled = false;
--    };
--}

-- https://ssl-config.mozilla.org/#server=haproxy&version=2.1&config=intermediate&openssl=1.1.0g&guideline=5.4
ssl = {
    protocol = "tlsv1_2+";
    ciphers = "ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384"
}

unlimited_jids = {
    "focus@auth.192.168.21.102",
    "jvb@auth.192.168.21.102"
}

VirtualHost "192.168.21.102"
    authentication = "token";
    app_id = "vclass.seskoad.org";
    app_secret = "vclass@2023";
    allow_empty_token = false;
    -- authentication = "jitsi-anonymous" -- do not delete me
    -- Properties below are modified by jitsi-meet-tokens package config
    -- and authentication above is switched to "token"
    --app_id="example_app_id"
    --app_secret="example_app_secret"
    -- Assign this host a certificate for TLS, otherwise it would use the one
    -- set in the global section (if any).
    -- Note that old-style SSL on port 5223 only supports one certificate, and will always
    -- use the global one.
    ssl = {
        key = "/etc/prosody/certs/192.168.21.102.key";
        certificate = "/etc/prosody/certs/192.168.21.102.crt";
    }
    av_moderation_component = "avmoderation.192.168.21.102"
    speakerstats_component = "speakerstats.192.168.21.102"
    conference_duration_component = "conferenceduration.192.168.21.102"
    end_conference_component = "endconference.192.168.21.102"
    -- we need bosh
    modules_enabled = {
        "bosh";
        "pubsub";
        "ping"; -- Enable mod_ping
        "speakerstats";
        "external_services";
        "conference_duration";
        "end_conference";
        "muc_lobby_rooms";
        "muc_breakout_rooms";
        "av_moderation";
        "room_metadata";
    }
    c2s_require_encryption = false
    lobby_muc = "lobby.192.168.21.102"
    breakout_rooms_muc = "breakout.192.168.21.102"
    room_metadata_component = "metadata.192.168.21.102"
    main_muc = "conference.192.168.21.102"
    -- muc_lobby_whitelist = { "recorder.192.168.21.102" } -- Here we can whitelist jibri to enter lobby enabled rooms

Component "conference.192.168.21.102" "muc"
    restrict_room_creation = true
    storage = "memory"
    modules_enabled = {
        "muc_hide_all";
        "muc_meeting_id";
        "muc_domain_mapper";
        "polls";
        --"token_verification";
        "muc_rate_limit";
        "muc_password_whitelist";
    }
    admins = { "focus@auth.192.168.21.102" }
    muc_password_whitelist = {
        "focus@auth.192.168.21.102"
    }
    muc_room_locking = false
    muc_room_default_public_jids = true

Component "breakout.192.168.21.102" "muc"
    restrict_room_creation = true
    storage = "memory"
    modules_enabled = {
        "muc_hide_all";
        "muc_meeting_id";
        "muc_domain_mapper";
        "muc_rate_limit";
        "polls";
    }
    admins = { "focus@auth.192.168.21.102" }
    muc_room_locking = false
    muc_room_default_public_jids = true

-- internal muc component
Component "internal.auth.192.168.21.102" "muc"
    storage = "memory"
    modules_enabled = {
        "muc_hide_all";
        "ping";
    }
    admins = { "focus@auth.192.168.21.102", "jvb@auth.192.168.21.102" }
    muc_room_locking = false
    muc_room_default_public_jids = true

VirtualHost "auth.192.168.21.102"
    ssl = {
        key = "/etc/prosody/certs/auth.192.168.21.102.key";
        certificate = "/etc/prosody/certs/auth.192.168.21.102.crt";
    }
    modules_enabled = {
        "limits_exception";
    }
    authentication = "internal_hashed"

-- Proxy to jicofo's user JID, so that it doesn't have to register as a component.
Component "focus.192.168.21.102" "client_proxy"
    target_address = "focus@auth.192.168.21.102"

Component "speakerstats.192.168.21.102" "speakerstats_component"
    muc_component = "conference.192.168.21.102"

Component "conferenceduration.192.168.21.102" "conference_duration_component"
    muc_component = "conference.192.168.21.102"

Component "endconference.192.168.21.102" "end_conference"
    muc_component = "conference.192.168.21.102"

Component "avmoderation.192.168.21.102" "av_moderation_component"
    muc_component = "conference.192.168.21.102"

Component "lobby.192.168.21.102" "muc"
    storage = "memory"
    restrict_room_creation = true
    muc_room_locking = false
    muc_room_default_public_jids = true
    modules_enabled = {
        "muc_hide_all";
        "muc_rate_limit";
        "polls";
    }

Component "metadata.192.168.21.102" "room_metadata_component"
    muc_component = "conference.192.168.21.102"
    breakout_rooms_component = "breakout.192.168.21.102"
